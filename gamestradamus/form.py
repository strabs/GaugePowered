"""Gauge related form tools
"""
import decimal

valid_digits = set("1234567890.,")
zero = decimal.Decimal("0.00")

#-------------------------------------------------------------------------------
def to_decimal(value, region):
    """Converts a form submitted decimal into a python decimal.

    return None if the value can't be converted
    """
    if value is None:
        return value
    value = value.strip()
    value_digits = set(value)
    if value_digits.issubset(valid_digits):

        value = value.replace(region['numbers'].thousands_separator, '')
        value = value.replace(region['numbers'].radix, '.')

        try:
            return decimal.Decimal(value)
        except (TypeError, decimal.InvalidOperation):
            return None
        if value < zero:
            return None

