Hey,

{{ user_account.nickname }} wants to compare your game Library with theirs on gaugepowered.com! You can accept this invitation by following this link: {{ invitation.accept_link() }} (Note that you have 5 days to accept this invitation before it expires.)

If you choose to accept this invitation, {{ user_account.nickname }} will be granted read-only access to your private Library. In exchange, you will be able to view their Library.

Gauge







If you do not want to receive any further communication, please click the following unsubscribe link to have this address removed from our e-mail list: 
{{ invitation.unsubscribe_link() }}
