define([], function() {

    var REGIONS = {
        'us': {
            'region': 'us',
            'dollars-precision': 2,
            'radix': '.',
            'thousands-separator': ',',
            'symbol': '$',
            'symbol-svg': '$',
            'symbol-position': 'before',
            'symbol-svg-position': 'before',
            'fraction-symbol': '¢',

            'format-dollars': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },
            'format-hours': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },
            'format-cost-per-hour': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },
            'format-rating': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                return x;
            },
            'format-integer': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },

            'parse-dollars': parseFloat,
            'parse-hours': parseFloat,

            'cost-filter-list': [
                {'label': '< $5', 'filter': '0-5'},
                {'label': '$5 - $10', 'filter': '5-10'},
                {'label': '$10 - $20', 'filter': '10-20'},
                {'label': '$20 - $40', 'filter': '20-40'},
                {'label': '$40+', 'filter': '40-10000'}
            ]
        },
        'ru': {
            'region': 'ru',
            'dollars-precision': 0,
            'radix': ',',
            'thousands-separator': '&nbsp;',
            'symbol': '<span class="ruble-sign">pуб</span>',
            'symbol-svg': ' pуб',
            'symbol-position': 'before',
            'symbol-svg-position': 'after',
            'fraction-symbol': 'коп.',

            'format-dollars': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                return x;
            },
            'format-hours': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                x = x.replace('.', ',');
                return x;
            },
            'format-cost-per-hour': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                x = x.replace('.', ',');
                return x;
            },
            'format-rating': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace('.', ',');
                return x;
            },
            'format-integer': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                return x;
            },

            'parse-dollars': function(x) {
                x = x.replace(',', '.');
                x = x.replace(' ', '');
                x = parseFloat(x);
                return x;
            },
            'parse-hours': function(x) {
                x = x.replace(',', '.');
                x = x.replace(' ', '');
                x = parseFloat(x);
                return x;
            },

            'cost-filter-list': [
                {'label': '< 150 pуб.', 'filter': '0-150'},
                {'label': '150 - 300 pуб.', 'filter': '150-300'},
                {'label': '300 - 500 pуб.', 'filter': '300-500'},
                {'label': '500 - 700 pуб.', 'filter': '500-700'},
                {'label': '700+ pуб.', 'filter': '700-10000'}
            ]
        },
        'uk': {
            'region': 'uk',
            'dollars-precision': 2,
            'radix': ',',
            'thousands-separator': ',',
            'symbol': '£',
            'symbol-svg': '£',
            'symbol-position': 'before',
            'symbol-svg-position': 'before',
            'fraction-symbol': '¢',
            
            'format-dollars': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            }, 
            'format-hours': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },
            'format-cost-per-hour': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },
            'format-rating': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                return x;
            },
            'format-integer': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return x;
            },

            'parse-dollars': parseFloat,
            'parse-hours': parseFloat,

            'cost-filter-list': [
                {'label': '< £5', 'filter': '0-5'},
                {'label': '£5 - £10', 'filter': '5-10'},
                {'label': '£10 - £20', 'filter': '10-20'},
                {'label': '£20 - £40', 'filter': '20-40'},
                {'label': '£40+', 'filter': '40-10000'}
            ]
        },
        'eu': {
            'region': 'eu',
            'dollars-precision': 2,
            'radix': ',',
            'thousands-separator': '&nbsp;',
            'symbol': '€',
            'symbol-svg': '€',
            'symbol-position': 'after',
            'symbol-svg-position': 'after',
            'fraction-symbol': 'c',

            'format-dollars': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                x = x.replace('.', ',');
                return x;
            },
            'format-hours': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                x = x.replace('.', ',');
                return x;
            },
            'format-cost-per-hour': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                x = x.replace('.', ',');
                return x;
            },
            'format-rating': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace('.', ',');
                return x;
            },
            'format-integer': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '&nbsp;');
                return x;
            },

            'parse-dollars': function(x) {
                x = x.replace(',', '.');
                x = x.replace(' ', '');
                x = parseFloat(x);
                return x;
            },
            'parse-hours': function(x) {
                x = x.replace(',', '.');
                x = x.replace(' ', '');
                x = parseFloat(x);
                return x;
            },

            'cost-filter-list': [
                {'label': '< 5€', 'filter': '0-5'},
                {'label': '5€ - 10€', 'filter': '5-10'},
                {'label': '10€ - 20€', 'filter': '10-20'},
                {'label': '20€ - 40€', 'filter': '20-40'},
                {'label': '40+€', 'filter': '40-10000'}
            ]
        },
        'br': {
            'region': 'br',
            'dollar-precision': 2,
            'radix': ',',
            'thousands-separator': '.',
            'symbol': 'R$',
            'symbol-svg': 'R$',
            'symbol-position': 'before',
            'symbol-svg-position': 'before',
            'fraction-symbol': '¢',
        
            'format-dollars': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', 'x');
                x = x.replace('.', ',');
                x = x.replace('x', '.');
                return x;
            }, 
            'format-hours': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', 'x');
                x = x.replace('.', ',');
                x = x.replace('x', '.');
                return x;
            },
            'format-cost-per-hour': function(x) {
                x = parseFloat(x);
                x = x.toFixed(2);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', 'x');
                x = x.replace('.', ',');
                x = x.replace('x', '.');
                return x;
            },
            'format-rating': function(x) {
                x = parseFloat(x);
                x = x.toFixed(1);
                x = x.replace('.', ',');
                return x;
            },
            'format-integer': function(x) {
                x = parseFloat(x);
                x = x.toFixed(0);
                x = x.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                x = x.replace(',', '.');
                return x;
            },

            'parse-dollars': function(x) {
                x = x.replace('.', '');
                x = x.replace(',', '.');
                x = parseFloat(x);
                return x;
            },
            'parse-hours': function(x) {
                x = x.replace('.', '');
                x = x.replace(',', '.');
                x = parseFloat(x);
                return x;
            },

            'cost-filter-list': [
                {'label': '< R$5', 'filter': '0-5'},
                {'label': 'R$5 - R$10', 'filter': '5-10'},
                {'label': 'R$10 - R$20', 'filter': '10-20'},
                {'label': 'R$20 - R$40', 'filter': '20-40'},
                {'label': 'R$40+', 'filter': '40-10000'}
            ]
        }
    } 
    REGIONS['au'] = REGIONS['us'];
    REGIONS['ua'] = REGIONS['us'];
    REGIONS['mx'] = REGIONS['us'];
    REGIONS['jp'] = REGIONS['us'];
    REGIONS['eu2'] = REGIONS['eu'];
    REGIONS['de'] = REGIONS['eu'];

    return REGIONS;
})
