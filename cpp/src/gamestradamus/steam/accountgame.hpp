//------------------------------------------------------------------------------
// Define a search engine.
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_STEAM_ACCOUNT_GAME_HPP
#define GAMESTRADAMUS_STEAM_ACCOUNT_GAME_HPP

#include <gamestradamus/steam/game.hpp>
#include <gamestradamus/consumer.hpp>

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>

namespace gamestradamus {
namespace steam {


// TODO: Make this inherit from ConsumerTrait
class AccountGame {
public:
    typedef double hours_t;

    //--------------------------------------------------------------------------
    AccountGame()
        : account_id(0), game_id(0), hours_played(0.0)
    {
    }

    //--------------------------------------------------------------------------
    AccountGame(gamestradamus::Consumer::id_t a_id, Game::id_t g_id, hours_t h_p)
        : account_id(a_id), game_id(g_id), hours_played(h_p)
    {
    }

    //--------------------------------------------------------------------------
    // the account we're associated with
    gamestradamus::Consumer::id_t account_id;

    //--------------------------------------------------------------------------
    // The Game we're associated with
    Game::id_t game_id;

    //--------------------------------------------------------------------------
    // The hours played
    hours_t hours_played;

    // This below isn't technically needed yet, but eventually we'll probably
    // have private variables in the future.
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & account_id;
        ar & game_id;
        ar & hours_played;
    }

};// end class AccountGame
// BOOST_CLASS_VERSION(AccountGame, 1)

}//end namespace steam
}//end namespace gamestradamus

#endif//GAMESTRADAMUS_STEAM_ACCOUNT_GAME_HPP

