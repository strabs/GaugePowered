#!/bin/bash
sudo apt-get install libpqxx3-dev libxml2-dev libxslt-dev 
virtualenv env -p python2.7 --no-site-packages --prompt="(gauge):"
source env/bin/activate
pip install -r requirements.txt
fab update

